//
// Created by a on 13/07/2023.
//

#ifndef PONG_GAME_BAR_H
#define PONG_GAME_BAR_H
#include "vector.h"

struct vector2 bar1;
struct vector2 bar2;
int barSize;

void initBars();
void updateBars();

#endif //PONG_GAME_BAR_H
