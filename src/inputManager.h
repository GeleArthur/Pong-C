#ifndef PONG_GAME_INPUTMANAGER_H
#define PONG_GAME_INPUTMANAGER_H

#include <stdbool.h>
#include <windows.h>
#include "consoleHandler.h"

void handleInput();

struct inputInfo {
    bool pressed;
    bool justPressed;
};

struct inputInfo upArrow;
struct inputInfo downArrow;
struct inputInfo w;
struct inputInfo s;

struct inputInfo esc;

#endif //PONG_GAME_INPUTMANAGER_H
