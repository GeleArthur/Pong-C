//
// Created by a on 14/06/2023.
//

#include "consoleHandler.h"

char *gameField;
const COORD topLeft = {0, 0};

void initConsole(){
    consoleOutPut = GetStdHandle(STD_OUTPUT_HANDLE);
    consoleInput = GetStdHandle(STD_INPUT_HANDLE);

    SetConsoleMode(consoleInput, ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT);
    screenSize();

    CONSOLE_CURSOR_INFO info;
    info.dwSize = 100;
    info.bVisible = FALSE;
    SetConsoleCursorInfo(consoleOutPut, &info);
}

bool screenSize(void)
{
    CONSOLE_SCREEN_BUFFER_INFO screenBufferInfo;

    GetConsoleScreenBufferInfo(consoleOutPut, &screenBufferInfo);

    if (screenInfo.width != screenBufferInfo.dwSize.X || screenInfo.height != screenBufferInfo.dwSize.Y)
    {
        screenInfo.width = screenBufferInfo.dwSize.X ;
        screenInfo.height = screenBufferInfo.dwSize.Y ;

        if (gameField != NULL) free(gameField);
        gameField = (char *)malloc(sizeof(char) * (screenInfo.width * screenInfo.height));
        return true;
    }
    return false;
}

bool drawOnConsole(char charter, const unsigned short x, const unsigned short y)
{
    if (x >= screenInfo.width || y >= screenInfo.height)
    {
        return 1;
    }

    gameField[(y * screenInfo.width) + x] = charter;
    return 0;
}

bool drawStringOnConsole(const char* string, const unsigned short x, const unsigned short y){
    size_t stringLen = strnlen(string, 30);

    for (int i = 0; i < stringLen; ++i) {
        drawOnConsole(string[i], x+i, y);
    }

    return 0;
}

void clearConsole(void) {
    memset(gameField, ' ', (screenInfo.width * screenInfo.height) * sizeof(char));
}

void draw(void){
    SetConsoleCursorPosition(consoleOutPut, topLeft );
    DWORD written;
    WriteConsoleA(consoleOutPut, gameField, (screenInfo.width * screenInfo.height), &written, NULL);
}