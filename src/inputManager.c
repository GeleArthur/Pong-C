//
// Created by a on 14/06/2023.
//

#include <stdio.h>
#include "inputManager.h"
//
//#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
//#define BYTE_TO_BINARY(byte)  \
//  ((byte) & 0x80 ? '1' : '0'), \
//  ((byte) & 0x40 ? '1' : '0'), \
//  ((byte) & 0x20 ? '1' : '0'), \
//  ((byte) & 0x10 ? '1' : '0'), \
//  ((byte) & 0x08 ? '1' : '0'), \
//  ((byte) & 0x04 ? '1' : '0'), \
//  ((byte) & 0x02 ? '1' : '0'), \
//  ((byte) & 0x01 ? '1' : '0')


void handleInput(){
    w.pressed = GetAsyncKeyState(0x57) & 0xFFFE ? true : false;
    s.pressed = GetAsyncKeyState(0x53) & 0xFFFE ? true : false;
    upArrow.pressed = GetAsyncKeyState(VK_UP) & 0xFFFE ? true : false;
    downArrow.pressed = GetAsyncKeyState(VK_DOWN) & 0xFFFE ? true : false;
}

void handleInputOld(){
    INPUT_RECORD irInBuf[128];
    DWORD cNumRead;
    DWORD out;
    GetNumberOfConsoleInputEvents(consoleInput, &out);

    if(out > 0){
        ReadConsoleInput(consoleInput, irInBuf, 128, &cNumRead);

        for (int i = 0; i < cNumRead; ++i) {
            switch (irInBuf[i].EventType) {
                case KEY_EVENT:
                    KEY_EVENT_RECORD key = irInBuf[i].Event.KeyEvent;
                    switch (key.wVirtualKeyCode) {
                        case VK_DOWN:
                            downArrow.pressed = key.bKeyDown;
                            break;
                        case VK_UP:
                            upArrow.pressed = key.bKeyDown;
                            break;
                        case VK_ESCAPE:
                            esc.pressed = true;
                            break;
//                        case 87:
//                            w.pressed = key.bKeyDown;
//                            break;
//                        case 83:
//                            s.pressed = key.bKeyDown;
                        default:
                            printf("%d=%d\n", key.wVirtualKeyCode, key.bKeyDown);
                            break;
                    }
                    break;
            }
        }
    }
}