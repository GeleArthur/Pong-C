#ifndef PONG_GAME_CONSOLEHANDLER_H
#define PONG_GAME_CONSOLEHANDLER_H

#include <windows.h>
#include <stdbool.h>
#include <stdio.h>

struct screenData{
    short width;
    short height;
};

HANDLE consoleOutPut, consoleInput;
struct screenData screenInfo;

void initConsole(void);
bool screenSize(void);
bool drawOnConsole(char charter, unsigned short x, unsigned short y);
bool drawStringOnConsole(const char* string, const unsigned short x, const unsigned short y);
void clearConsole(void);
void draw(void);

#endif //PONG_GAME_CONSOLEHANDLER_H
