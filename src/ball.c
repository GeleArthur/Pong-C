#include "ball.h"
#include "consoleHandler.h"
#include "math.h"
#include "bar.h"

struct vector2 ballPosition = {30,10};
struct vector2 ballSize = {4,3};
struct vector2 ballVelocity = {-0.5f,0.25f};

void updateBall(){
    ballPosition.x += ballVelocity.x;
    ballPosition.y += ballVelocity.y;

    if(ballPosition.x + ballSize.x/2 > (float)screenInfo.width){
        ballVelocity.x = -ballVelocity.x;
        ballPosition.x = (float)screenInfo.width - 1 - ballSize.x/2;
    }
    if(ballPosition.x - ballSize.x/2 < 0){
        ballVelocity.x = -ballVelocity.x;
        ballPosition.x = ballSize.x;
    }

    if(ballPosition.y + ballSize.y/2 > (float)screenInfo.height-1){
        ballVelocity.y = -ballVelocity.y;
        ballPosition.y = (float)screenInfo.height-1-ballSize.y/2;
    }
    if(ballPosition.y - ballSize.y/2 < 1){
        ballVelocity.y = -ballVelocity.y;
        ballPosition.y = ballSize.y;
    }

    if(ballPosition.x - ballSize.x/2 < bar1.x && ballPosition.y < bar1.y+barSize && ballPosition.y > bar1.y-barSize){
        ballVelocity.x = -ballVelocity.x;
        ballPosition.x = bar1.x + ballSize.x;

        ballVelocity.x +=0.3;
        ballVelocity.y +=0.3;
    }

    if(ballPosition.x + ballSize.x/2 > bar2.x && ballPosition.y < bar2.y+barSize && ballPosition.y > bar2.y-barSize){
        ballVelocity.x = -ballVelocity.x;
        ballPosition.x = bar2.x - ballSize.x;

        ballVelocity.x -=0.3;
        ballVelocity.y -=0.3;
    }
}

void drawBall(){
    drawStringOnConsole(" .-. ", (unsigned short)floorf(ballPosition.x-ballSize.x/2), (unsigned short)floorf((ballPosition.y)-1));
    drawStringOnConsole("|   |", (unsigned short)floorf(ballPosition.x-ballSize.x/2), (unsigned short)floorf(ballPosition.y));
    drawStringOnConsole(" `-` ", (unsigned short)floorf(ballPosition.x-ballSize.x/2), (unsigned short)floorf((ballPosition.y)+1));
}