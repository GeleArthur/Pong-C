//
// Created by a on 13/07/2023.
//

#include "bar.h"
#include "inputManager.h"
#include "consoleHandler.h"

int barSize = 3;
struct vector2 bar1;
struct vector2 bar2;

void initBars() {
    bar1.x = 5;
    bar1.y = (float)screenInfo.height/2.0f;
    bar2.y = (float)screenInfo.height/2.0f;
    bar2.x = (float)screenInfo.width-6.0f;
}

void updateBars() {
    if(upArrow.pressed){
        bar2.y--;
    }
    if(downArrow.pressed){
        bar2.y++;
    }

    if(w.pressed){
        bar1.y--;
    }
    if(s.pressed){
        bar1.y++;
    }

    if(bar1.y + barSize > screenInfo.height){
        bar1.y = screenInfo.height - barSize+1;
    }
    else
    if(bar1.y - barSize < 0){
        bar1.y = barSize;
    }

    if(bar2.y + barSize > screenInfo.height){
        bar2.y = screenInfo.height - barSize+1;
    }
    else
    if(bar2.y - barSize < 0){
        bar2.y = barSize;
    }

}