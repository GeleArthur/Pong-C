#ifndef PONG_GAME_BALL_H
#define PONG_GAME_BALL_H

#include "vector.h"

struct vector2 ballPosition;
struct vector2 ballSize;
struct vector2 ballVelocity;

void updateBall();
void drawBall();

#endif //PONG_GAME_BALL_H
