#include <stdbool.h>
#include <windows.h>
#include <math.h>

#include "ball.h"
#include "bar.h"
#include "inputManager.h"
#include "consoleHandler.h"

LARGE_INTEGER frequency, startTime, endTime;
double deltaTime;

void drawWalls(){
    for (int i = 0; i < screenInfo.width; ++i) {
        drawOnConsole('-', i, 0);
    }
    for (int i = 0; i < screenInfo.width; ++i) {
        drawOnConsole('-', i, screenInfo.height-1);
    }
}

int main(void)
{
    Sleep(100);

    initConsole();
    initBars();
    bool running = true;

	QueryPerformanceFrequency(&frequency);
	while (running)
	{
		QueryPerformanceCounter(&startTime);
        handleInput();
        screenSize();

        // GAME LOGIC

        if(esc.pressed == true){
            running = false;
        }

        updateBall();
        updateBars();

        // DRAWING

        clearConsole();
        drawWalls();
        drawBall();

//        drawOnConsole('@', (unsigned short)floorf(ballPosition.x), (unsigned short)floorf(ballPosition.y));

        float offset = barSize/2.0f;

        for (int i = 0; i < barSize; ++i) {
            drawOnConsole('|', bar1.x, bar1.y + i-offset);
        }

        for (int i = 0; i < barSize; ++i) {
            drawOnConsole('|', bar2.x, bar2.y + i-offset);
        }
        

        draw();

		QueryPerformanceCounter(&endTime);
		deltaTime = ((double) (endTime.QuadPart - startTime.QuadPart)) / ((double)frequency.QuadPart) ;

        double timeToWait = 1.0/60.0 - deltaTime;
        if(timeToWait > 0){
            Sleep((long)(timeToWait * 1000));
        }
	}

	return 0;
}